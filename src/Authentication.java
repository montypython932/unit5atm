import javax.swing.JOptionPane;

public class Authentication {
	
	int counter = 3;
	
	public boolean authenticate(String pass, String input) {
		
		boolean canStillAttempt = true;
		
		//If pin codes match, login
		if (pass.equalsIgnoreCase(input)) {
			JOptionPane.showMessageDialog(null, "Welcome!");
			return canStillAttempt;
		//If they mismatch but there are still attempts try again
		} else if (!pass.equalsIgnoreCase(input) && counter > 0) {
			JOptionPane.showMessageDialog(null, "You have " + counter + " attempts left");
			counter--;
			return canStillAttempt;
		//Maximum attempts reached, can no longer attempt to log in
		} else {
			JOptionPane.showMessageDialog(null, "You've reached the maximum amount of attempts");
			canStillAttempt = false;
			return canStillAttempt;
		}
	}
}
