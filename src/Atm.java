import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JPasswordField;

public class Atm extends JFrame {

	String pin = "1234";
	String input = "";
	
	JButton button1 = new JButton();
	JButton button2 = new JButton();
	JButton button3 = new JButton();
	JButton button4 = new JButton();
	JButton button5 = new JButton();
	JButton button6 = new JButton();
	JButton button7 = new JButton();
	JButton button8 = new JButton();
	JButton button9 = new JButton();
	JButton clear = new JButton();
	JButton button0 = new JButton();
	JButton enter = new JButton();
	JPasswordField pass = new JPasswordField(4);
	
	
	public Atm() {
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setSize(800, 600);
		setTitle("ATM PIN AUTHENTICATION");
		JPanel passPanel = new JPanel();
		passPanel.add(pass);
		//ActionListeners
		buttonAction numInput = new buttonAction();
		enterAction enterInput = new enterAction();
		clearAction clearInput = new clearAction();
		//For the buttons, their JPanel and their layout
		GridLayout gridButtons = new GridLayout(4, 3);
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(gridButtons);
		//Sets buttons texts, adds actionlisteners and adds them to panel
		button1.addActionListener(numInput);
		button1.setText("1");
		button2.addActionListener(numInput);
		button2.setText("2");
		button3.addActionListener(numInput);
		button3.setText("3");
		button4.addActionListener(numInput);
		button4.setText("4");
		button5.addActionListener(numInput);
		button5.setText("5");
		button6.addActionListener(numInput);
		button6.setText("6");
		button7.addActionListener(numInput);
		button7.setText("7");
		button8.addActionListener(numInput);
		button8.setText("8");
		button9.addActionListener(numInput);
		button9.setText("9");
		clear.addActionListener(clearInput);
		clear.setText("Clear");
		button0.addActionListener(numInput);
		button0.setText("0");
		enter.addActionListener(enterInput);
		enter.setText("Enter");
		JButton[] buttons = {button1, button2, button3, button4, button5, button6, button7, button8, button9, clear, button0, enter};
		for (int i = 0; i < buttons.length; i++) {
			buttonPanel.add(buttons[i]);
		}
		add(pass, BorderLayout.NORTH);
		add(buttonPanel, BorderLayout.CENTER);
		setVisible(true);
	}
	
	public static void main(String[] args) {
		Atm atm = new Atm();
	}
	
	//for numbers 0-9
	public class buttonAction implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (input.length() < 4) {
				input += e.getActionCommand();
				pass.setText(input);
			}
			System.out.println(input);
		}
	}
	
	//for the clear button
	public class clearAction implements ActionListener {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource().equals(clear)) {
				pass.setText("");
				input = "";
			}
		}
		
	}
	
	//for the enter button
	public class enterAction implements ActionListener {

		Authentication auth = new Authentication();
		
		@Override
		public void actionPerformed(ActionEvent e) {
			if(e.getSource().equals(enter)) {
				//If maximum attempts are reached, enter button no longer works
				if(auth.authenticate(pin, input) == false) {
					enter.setEnabled(false);
				}
			}
		}
	}
}
